import update from 'immutability-helper';
import React, { useCallback, useState } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import styled, { css } from 'styled-components';
import './App.css';
import CardList from './CardList';
import StepList from './StepList';
import { ICard } from './types';

const Wrapper = styled.div`
  display: flex;
  align-items: flex-start;
`;

const LeftSide = styled.div`
  background-color: red;
  height: 100vh;
  width: 400px;
`;

const RightSide = styled.div`
  flex-grow: 1;
  background-color: green;
  height: 100vh;
  overflow-y: scroll;
`;

const DEFAULT_CARDS = [1, 2, 3, 4, 5, 6, 7, 8, 9].map((id) => ({
  id,
  name: `card-${id}`,
}));

const CARDS_BY_ID: any = {};

DEFAULT_CARDS.forEach((c) => {
  CARDS_BY_ID[c.id] = c;
});

function shuffle<T>(array: T[]) {
  const res = [...array];
  var currentIndex = res.length,
    temporaryValue,
    randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = res[currentIndex];
    res[currentIndex] = res[randomIndex];
    res[randomIndex] = temporaryValue;
  }

  return res;
}

function App() {
  const [cards, setCards] = useState<ICard[]>(
    [1, 2, 3, 4, 5].map((id) => ({ id, name: `card-${id}` }))
  );

  const moveCard = useCallback(
    (dragIndex: number, hoverIndex: number) => {
      const dragCard = cards[dragIndex];
      setCards(
        update(cards, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard],
          ],
        })
      );
    },
    [cards]
  );

  const deleteCard = useCallback(
    (index: number) => {
      setCards(
        update(cards, {
          $splice: [[index, 1]],
        })
      );
    },
    [cards]
  );

  const addCard = useCallback(
    () =>
      setCards(
        update(cards, {
          $push: [{ id: cards.length + 1, name: `card-${cards.length + 1}` }],
        })
      ),
    [cards]
  );

  function shuffleCards() {
    setCards(shuffle(cards));
  }

  return (
    <DndProvider backend={HTML5Backend}>
      <Wrapper>
        <LeftSide>
          <StepList deleteCard={deleteCard} />
        </LeftSide>
        <RightSide>
          <CardList
            shuffleCards={shuffleCards}
            addCard={addCard}
            deleteCard={deleteCard}
            moveCard={moveCard}
            cards={cards}
          />
        </RightSide>
      </Wrapper>
    </DndProvider>
  );
}

export default App;
