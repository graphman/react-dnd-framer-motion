import { motion } from 'framer-motion';
import React, { useRef } from 'react';
import { useDrag, useDrop, XYCoord } from 'react-dnd';
import styled from 'styled-components';
import { DragItem, ICard } from './types';

const Wrapper = styled(motion.div)`
  width: 100%;
  padding-bottom: 10px;
  padding-top: 10px;
`;

const InnerWrapper = styled.div`
  background-color: yellow;
  width: 100%;
`;

const HoverIndicator = styled.div`
  width: 100%;
  height: 50px;
  border-radius: 16px;
  background-color: #ccc;
  margin-top: 10px;
  margin-bottom: -10px;
`;

interface CardProps {
  card: ICard;
  index: number;
  hovering: boolean;
  setHoverIndex: (value: number | null) => void;
  moveCard: (fromIndex: number, toIndex: number) => void;
  deleteCard: (index: number) => void;
}

function Card(props: CardProps) {
  const ref = useRef<HTMLDivElement>(null);

  const [, drag] = useDrag({
    end: () => props.setHoverIndex(null),
    item: { id: props.card.id, index: props.index, type: 'card' },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const [, drop] = useDrop({
    accept: 'card',
    hover: (item: DragItem, monitor) => {
      if (!ref.current) return;
      const hoverIndex = props.index;

      const hoverBoundingRect = ref.current?.getBoundingClientRect();

      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

      const clientOffset = monitor.getClientOffset();

      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top;

      if (hoverClientY > hoverMiddleY) {
        if (hoverIndex === item.index) return;
        props.setHoverIndex(hoverIndex);
      } else {
        if (hoverIndex - 1 === item.index) return;
        props.setHoverIndex(hoverIndex - 1);
      }
    },
  });

  drag(drop(ref));

  return (
    <Wrapper
      layout="position"
      initial={{ opacity: 0, y: 50, scale: 0.9 }}
      animate={{ opacity: 1, y: 0, scale: 1 }}
      exit={{ opacity: 0, scale: 0.8, transition: { duration: 0.2 } }}
      transition={{ layoutY: { duration: 0.2 } }}
      ref={ref}
    >
      <InnerWrapper style={{ height: `${50 * props.card.id}px` }}>
        {props.card.name}
        <span onClick={() => props.deleteCard(props.index)}>X</span>
      </InnerWrapper>
      {props.hovering && <HoverIndicator />}
    </Wrapper>
  );
}

export default Card;
