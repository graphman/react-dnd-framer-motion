import { AnimatePresence } from 'framer-motion';
import React, { useRef, useState } from 'react';
import { useDrop } from 'react-dnd';
import styled from 'styled-components';
import Card from './Card';
import { DragItem, ICard } from './types';

const Wrapper = styled.div`
  padding: 20px;
  max-width: 800px;
`;

const Indicator = styled.div<{ hovering: boolean }>`
  width: 100%;

  padding-bottom: 10px;
  padding-top: 10px;

  box-sizing: border-box;

  ${(p) =>
    p.hovering &&
    `
    border-bottom: 40px solid transparent;
    border-color: blue;
  `}
`;

interface CardListProps {
  cards: ICard[];
  moveCard: (fromIndex: number, toIndex: number) => void;
  deleteCard: (index: number) => void;
  addCard: () => void;
  shuffleCards: () => void;
}

function CardList({
  moveCard,
  cards,
  deleteCard,
  addCard,
  shuffleCards,
}: CardListProps) {
  const ref = useRef<HTMLDivElement | null>(null);
  const [hoverIndex, setHoverIndex] = useState<number | null>(null);

  const [, drop] = useDrop({
    accept: 'card',
    drop: (item: DragItem) => {
      if (hoverIndex === null || hoverIndex === item.index) {
        return;
      }

      if (item.index >= hoverIndex) {
        moveCard(item.index, hoverIndex + 1);
      } else {
        moveCard(item.index, hoverIndex);
      }
    },
  });

  drop(ref);

  return (
    <Wrapper ref={ref}>
      <button onClick={addCard}>Add card</button>
      <button onClick={shuffleCards}>Shuffle</button>
      <Indicator hovering={hoverIndex === -1} />
      <AnimatePresence initial={false}>
        {cards.map((card, index) => (
          <Card
            hovering={index === hoverIndex}
            setHoverIndex={setHoverIndex}
            card={card}
            deleteCard={deleteCard}
            moveCard={moveCard}
            index={index}
            key={`card-${card.id}`}
          />
        ))}
      </AnimatePresence>
    </Wrapper>
  );
}

export default CardList;
