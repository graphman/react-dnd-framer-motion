import React, { useRef } from 'react';
import { useDrag, useDrop, DragPreviewOptions } from 'react-dnd';
import styled from 'styled-components';
import { IStep } from './types';
import update from 'immutability-helper';

const Wrapper = styled.div<{ hovering: boolean }>`
  background-color: blue;
  width: 100%;
  padding: 20px;
  box-sizing: border-box;
  margin-bottom: 20px;

  ${(p) => p.hovering && `background-color: purple`}
`;

interface StepProps {
  step: IStep;
  index: number;
  moveStep: (fromIndex: number, toIndex: number) => void;
  deleteCard: (index: number) => void;
}

function Step(props: StepProps) {
  const ref = useRef<HTMLDivElement>(null);

  const [{ isHovering }, cardDrop] = useDrop({
    accept: 'card',
    collect: (monitor) => ({
      isHovering: monitor.isOver(),
    }),
    drop: (item: { index: number; type: string }) => {
      props.deleteCard(item.index);
    },
  });

  cardDrop(ref);

  return (
    <Wrapper hovering={isHovering} ref={ref}>
      step {props.step.id}
    </Wrapper>
  );
}

export default Step;
