import React, { useCallback, useState } from 'react';
import styled from 'styled-components';
import Step from './Step';
import { IStep } from './types';
import update from 'immutability-helper';

const Wrapper = styled.div`
  padding: 20px;
`;

interface StepListProps {
  deleteCard: (index: number) => void;
}

function StepList(props: StepListProps) {
  const [steps, stepSteps] = useState<IStep[]>(
    [1, 2, 3, 4, 5, 6, 7, 8, 9].map((id) => ({ id, name: `card-${id}` })),
  );
  const moveStep = useCallback(
    (dragIndex: number, hoverIndex: number) => {
      const dragCard = steps[dragIndex];
      stepSteps(
        update(steps, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard],
          ],
        }),
      );
    },
    [steps],
  );

  return (
    <Wrapper>
      {steps.map((step, index) => (
        <Step
          key={`step-${index}`}
          step={step}
          index={index}
          moveStep={moveStep}
          deleteCard={props.deleteCard}
        />
      ))}
    </Wrapper>
  );
}

export default StepList;
