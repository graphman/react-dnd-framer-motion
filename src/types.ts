export interface ICard {
  id: number;
  name: string;
}

export interface IStep {
  id: number;
  name: string;
}

export interface DragItem {
  id: number;
  index: number;
  type: string;
}
